package org.example;

import lombok.extern.slf4j.Slf4j;
import org.example.Service.myService;
import org.example.Util.ResponseAPI;
import org.example.Util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
@Slf4j
public class MyTask4 {
    private final myService myService;
    @Autowired
    public MyTask4(myService myService) {
        this.myService = myService;
    }
    public static void main(String[] args) {
        SpringApplication.run(MyTask4.class, args);
        String token = Util.getToken();
        log.info(token);
    }
    @GetMapping("/{token}")
        public ResponseAPI getToken(@PathVariable String token){
        return myService.dosomething(token);
    }
//    @GetMapping
//    public ResponseAPI gettoken1(@RequestBody String token){
//        return myService.dosomething(token);
//    }
}