package org.example.Service;

import lombok.extern.slf4j.Slf4j;
import org.example.Util.ResponseAPI;
import org.example.Util.Util;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class myService {

    public ResponseAPI dosomething(String token){
        boolean isValidToken = Util.isValidToken(token);
        if(isValidToken){
            log.info("Token valid");
            return new ResponseAPI(true,"Token valid");
        }
        log.info("Your token is expired");
        String newToken = Util.getToken();
        log.info("Your new token is: "+newToken);
        return new ResponseAPI(false,"Token expired.Your new token is: "+newToken);
    }

}
