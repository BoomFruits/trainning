package org.example.Util;

import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

@Component
public class Util {
        private static String token;
        private static Long expiredTime;
        public static Random random = new Random();
        private static final Map<String,Long> mem = new HashMap<>();
        public static String getToken(){
                token = getAnyTokenValid();
                if(Objects.equals(token, null)) {
                    token = generateToken();
                    expiredTime = System.currentTimeMillis() + 10000; //10000 millis = 10 second
                    mem.put(token,expiredTime);
                }
                return token;
        }
        public static boolean isValidToken(String token){
                Long expiredTime = mem.get(token);
                System.out.println("Expired time : "+expiredTime);
                return expiredTime != null && expiredTime > System.currentTimeMillis();
        }

        public static String generateToken(){
//                String token = getAnyTokenValid();
//                System.out.println(token);
                 return String.format("%06d",random.nextInt(1000000));
        }
        public static String getAnyTokenValid(){
                if(mem.isEmpty())
                        return null;
                for(Map.Entry<String, Long> entry:mem.entrySet()){
                        String currentToken = entry.getKey();
                        if(isValidToken(currentToken)){ //kiem tra trong mem xem có token nào vẫn valid thì dùng tiếp không phai tạo mới
                                return currentToken;
                        }
                        mem.remove(currentToken);
                }
                return null;
        }
}
